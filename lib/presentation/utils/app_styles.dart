import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kBlack = const Color(0xFF000000);
Color kWhite = const Color(0xFFFFFFFF);
Color kPrimary = const Color(0xFF1D61E7);
Color kGrey = const Color(0xFF6C7278);
Color kStroke = const Color.fromARGB(255, 204, 231, 246);

final kJakartaBold = GoogleFonts.plusJakartaSans(
  fontWeight: FontWeight.w700,
);

final kJakartaSemibold = GoogleFonts.plusJakartaSans(
  fontWeight: FontWeight.w600,
);

final kJakartaMedium = GoogleFonts.plusJakartaSans(
  fontWeight: FontWeight.w500,
);

final kJakartaRegular = GoogleFonts.plusJakartaSans(
  fontWeight: FontWeight.w400,
);

final kJakartaLight = GoogleFonts.plusJakartaSans(
  fontWeight: FontWeight.w300,
);
