part of 'app_route.dart';

abstract class RouteName {
  static const onboardingPage = '/onboardingPage';
  static const loginPage = '/loginPage';
  static const homePage = '/homePage';
  static const assetPage = '/assetPage';
  static const assetInputPage = '/assetInputPage';
  static const assetEditPage = '/assetEditPage';
  static const assetDetailPage = '/assetDetailPage';
  static const locationPage = '/locationPage';
  static const locationInputPage = '/locationInputPage';
  static const locationEditPage = '/locationEditPage';
  static const locationDetailPage = '/locationDetailPage';
  static const movingPage = '/movingPage';
  static const showImagePage = '/showImagePage';
}
