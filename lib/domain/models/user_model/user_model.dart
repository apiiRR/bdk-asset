import 'package:freezed_annotation/freezed_annotation.dart';

import 'result.dart';

part 'user_model.freezed.dart';
part 'user_model.g.dart';

@freezed
class UserModel with _$UserModel {
  factory UserModel({
    Result? result,
    dynamic id,
    String? jsonrpc,
  }) = _UserModel;

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);
}
