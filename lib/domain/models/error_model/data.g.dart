// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$DataImpl _$$DataImplFromJson(Map<String, dynamic> json) => _$DataImpl(
      name: json['name'].toString(),
      debug: json['debug'].toString(),
      message: json['message'].toString(),
      arguments: (json['arguments'] as List<dynamic>?)
          ?.map((e) => e.toString())
          .toList(),
      exceptionType: json['exception_type'].toString(),
    );

Map<String, dynamic> _$$DataImplToJson(_$DataImpl instance) =>
    <String, dynamic>{
      'name': instance.name,
      'debug': instance.debug,
      'message': instance.message,
      'arguments': instance.arguments,
      'exception_type': instance.exceptionType,
    };
