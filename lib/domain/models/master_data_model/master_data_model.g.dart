// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'master_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$MasterDataModelImpl _$$MasterDataModelImplFromJson(
        Map<String, dynamic> json) =>
    _$MasterDataModelImpl(
      jsonrpc: json['jsonrpc'].toString(),
      id: json['id'],
      result: json['result'] == null
          ? null
          : Result.fromJson(json['result'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$MasterDataModelImplToJson(
        _$MasterDataModelImpl instance) =>
    <String, dynamic>{
      'jsonrpc': instance.jsonrpc,
      'id': instance.id,
      'result': instance.result,
    };
